#!/bin/env bash

image_name=${1}

url="https://registry.hub.docker.com/v2/repositories/${image_name}/tags?page_size=100"

while [[ "${url}" != "null" ]]
do
    tags=$(curl -s "${url}")
    echo "${tags}" | jq -r ".results[].name"
    url="$(echo "${tags}" | jq -r '.next')"
done