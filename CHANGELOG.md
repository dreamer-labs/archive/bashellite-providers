## [1.5.1](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/compare/v1.5.0...v1.5.1) (2020-05-18)


### Bug Fixes

* Correct issue [#5](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/issues/5) ([3ece016](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/commit/3ece016))

# [1.5.0](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/compare/v1.4.0...v1.5.0) (2020-04-24)


### Features

* Add additional dev scripts ([ff2dccd](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/commit/ff2dccd))
* Add regex tags to podman provider ([d0828ca](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/commit/d0828ca))
* Update podman examples ([ddb1750](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/commit/ddb1750))
* Update podman provider installer ([0e12cbd](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/commit/0e12cbd))

# [1.4.0](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/compare/v1.3.2...v1.4.0) (2020-04-03)


### Features

* Add draft dev scripts ([e9fc5f9](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/commit/e9fc5f9))

## [1.3.2](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/compare/v1.3.1...v1.3.2) (2020-04-01)


### Bug Fixes

* Update pypi provider_wrapper.sh ([241ad5b](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/commit/241ad5b))

## [1.3.1](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/compare/v1.3.0...v1.3.1) (2020-01-29)


### Bug Fixes

* Correct git provider repo update error ([eeb09f0](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/commit/eeb09f0))

# [1.3.0](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/compare/v1.2.0...v1.3.0) (2020-01-03)


### Bug Fixes

* change to pip3 ([266d8c0](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/commit/266d8c0))
* return appropriate error codes ([859931a](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/commit/859931a))


### Features

* add initial files ([0e7b03f](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/commit/0e7b03f))

# [1.2.0](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/compare/v1.1.0...v1.2.0) (2019-12-31)


### Features

* updated bandersnatch version ([7eeb276](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/commit/7eeb276))
* updated bandersnatch version ([167ffd1](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/commit/167ffd1))

# [1.1.0](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/compare/v1.0.1...v1.1.0) (2019-12-31)


### Features

* updated bandersnatch version ([c2c1590](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/commit/c2c1590))

## [1.0.1](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/compare/v1.0.0...v1.0.1) (2019-12-23)


### Bug Fixes

* correct stale file issue ([d7fb0bd](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/commit/d7fb0bd))

# 1.0.0 (2019-11-06)


### Bug Fixes

* Added .mdlrc with rule exceptions ([45cb0bd](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/commit/45cb0bd))
* Corrected markdownlint error ([bfc7b4c](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/commit/bfc7b4c))
* update git provider ([c0b64b4](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/commit/c0b64b4))


### Features

* Bootstrap CI ([ad04fd7](https://gitlab.com/dreamer-labs/bashellite/bashellite-providers/commit/ad04fd7))
